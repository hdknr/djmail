.. Django and Mail documentation master file, created by
   sphinx-quickstart on Sun Jan 27 15:29:33 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Django and Mail's documentation!
===========================================

Contents:

.. toctree::
    :maxdepth: 2

    core_mail

    init
    
    message

    backends_smtp
    backends_locmem
    backends_console
    backends_filebased

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

