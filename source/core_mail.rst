===================
django.core.mail 
===================

.. contents::
    :local:

ファイル構造
==============

- ならびに1.4 から 1.5 の修正ポイント

::
    
    Django-1.5c1/django/core/mail/
    ├── __init__.py   ( 主に Python3の互換性。 send_mail_mass でEmailMessageのコンストラクタに connectionを指定するようになった )
    ├── backends
    │   ├── __init__.py
    │   ├── base.py
    │   ├── console.py    ( 排他ロックのロック/リリースを withブロックでやるようにしました。以前は acquire()/release()をよびだしていました )
    │   ├── dummy.py
    │   ├── filebased.py ( django.utils.six を使って、fileのパス名のタイプチェックを six.string_types でやるようにしました )
    │   ├── locmem.py　　　( 個別メールのヘッダーを検証するようにした )
    │   └── smtp.py ( sslライブラリ, _sendに渡すメールメッセージテキストをforce_bytes化。そのためのメッセージからcharset取得するのだが、デフォルトutf-8 )
    ├── message.py ( リテラルのunicode化、 six使って 2-3 互換性 。 force_text() を使うように変更 ( force_unicodeから )   など )
    └── utils.py
    


