========================================
django.core.maill.backends.smtp
========================================

.. contents::
    :local:

Djang 1.4 -> 1.5RC
=======================

socket->ssl
-------------------

- http://docs.python.org/2/library/ssl.html
- socket -> ssl化したので、例外も変わった

:: 

    3c3

    < import socket
    ---
    > import ssl 
    
::

    65c68
    < except socket.sslerror:
    ---
    > except (ssl.SSLError, smtplib.SMTPServerDisconnected):
    

force_bytesを使います
---------------------------------------

- force_bytes は django.utils.encoding.smart_bytesとほとんどおなじですが、lazy なインスタンスを文字列化します ( smart_bytesではそのままlazyインスタンスです)

::

    9a10
    > from django.utils.encoding import force_bytes

::

   110c113
   < email_message.message().as_string())
   ---
   > force_bytes(message.as_string(), charset))

接続が初期化されていない時は、close処理をしないで帰るようにしました
---------------------------------------------------------------------------------

::

    61a63,64
    > if self.connection is None:
    > return

コメント追加
-----------------

- when the connection was already disconnected by the server. が追加された

::

    67c70,71
    < # sometimes.
    ---
    > # sometimes, or when the connection was already disconnected
    > # by the server.
    
with 文で排他処理するようにした
------------------------------------

::

    83,84c87
    < self._lock.acquire()
    < try:
    ---
    > with self._lock:
    97,98d99
    < finally:
    < self._lock.release()

force_bytesするために文字変換のcharsetをメッセージから取り出す
------------------------------------------------------------------------

- デフォルトはutf-8
- settingsからとらないのか？

::

    107a109,110
    > message = email_message.message()
    > charset = message.get_charset().get_output_charset() if message.get_charset() else 'utf-8'

