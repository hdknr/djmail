==================================================
django.core.mail.backends.console
==================================================

.. contents::
    :local:


Django 1.4 -> 1.5 RC
===========================

- 例外処理ブロックに入る前に、 ロックを withするようにしました。
- 1.4では、 _lock.acquire() で開始し、 _lock.release() で終了するようにしていました。


.. code-block:: python

    def send_messages(self, email_messages):
        """Write all messages to the stream in a thread-safe way."""

        if not email_messages:
            return

        with self._lock:
            try:
                stream_created = self.open()

                for message in email_messages:
                    self.stream.write('%s\n' % message.message().as_string())
                    self.stream.write('-' * 79) 
                    self.stream.write('\n')
                    self.stream.flush() # flush after each message

                if stream_created:
                    self.close()

            except:
                if not self.fail_silently:
                    raise

        return len(email_messages)

