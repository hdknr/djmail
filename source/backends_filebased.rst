=================================================
django.core.mail.backends.filebased
=================================================

.. contents::
    :local:

Django 1.4 -> 1.5 RC
========================

sixでPython3互換
--------------------

::

    18c19
    < if not isinstance(self.file_path, basestring):
    ---
    > if not isinstance(self.file_path, six.string_types):

例外処理に as を使う
------------------------

::

    28c29
    < except OSError, err:
    ---
    > except OSError as err:



