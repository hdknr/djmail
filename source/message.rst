================================================
django.core.mail.message
================================================

.. contents::
    :local:

1.4から1.5RCへの変更
=========================

リテラルを全部ユニコードにします 
----------------------------------------

- http://note.harajuku-tech.org/from-future-import-unicodeliterals-unicode )

::

    0a1,2

    > from __future__ import unicode_literals
    > 

    3a6
    > import sys


::

    107,108c112,113

    < if u'@' in addr:
    < localpart, domain = addr.split(u'@', 1)
    
    ---

    > if '@' in addr:
    > localpart, domain = addr.split('@', 1)


force_unicode -> force_text  / sixで Python 2-3の互換性
------------------------------------------------------------------

::

    15c18,19

    < from django.utils.encoding import smart_str, force_unicode

    ---
    
    > from django.utils.encoding import force_text
    
    > from django.utils import six

::

    82c82
    
    < val = force_unicode(val)
    
    ---
    
    > val = force_text(val)

::

    184c192

    < assert not isinstance(to, basestring), '"to" argument must be a list or tuple'

    ---

    > assert not isinstance(to, six.string_types), '"to" argument must be a list or tuple'

::

    
    189c197
    
    < assert not isinstance(cc, basestring), '"cc" argument must be a list or tuple'
    
    ---
    
    > assert not isinstance(cc, six.string_types), '"cc" argument must be a list or tuple'

::
    
    194c202
    
    < assert not isinstance(bcc, basestring), '"bcc" argument must be a list or tuple'
    
    ---
    
    > assert not isinstance(bcc, six.string_types), '"bcc" argument must be a list or tuple'
    

StringIOは six.StringIOをつかうようになりました
------------------------------------------------

::

   17,20d20
   
   < try:
   < from cStringIO import StringIO
   < except ImportError:
   < from StringIO import StringIO


::

    135c140
    
    < fp = StringIO()
    
    ---
    
    > fp = six.StringIO()

::

    159c167
    
    < fp = StringIO()
    
    ---
    
    > fp = six.StringIO()


encodeのやり方を Python3互換になるように
---------------------------------------------------

::

    86c86
    
    < val = val.encode('ascii')
    
    ---
    
    > val.encode('ascii')
    
::

    92c92
    
    < val = str(Header(val, encoding))
    
    ---
    
    > val = Header(val, encoding).encode()
    
::    
    
    95,96c95,96
    
    < val = Header(val)
    
    < return name, val
    
    ---
    
    > val = Header(val).encode()
    
    > return str(name), val
    
    
force_unicode() -> force_text
------------------------------------

::

    100,101c100,101
    
    < if isinstance(addr, basestring):
    < addr = parseaddr(force_unicode(addr))
    
    ---
    
    > if isinstance(addr, six.string_types):
    > addr = parseaddr(force_text(addr))


ヘッダーのエンコード化の変更
------------------------------------------------


::

    103c103,108

    < nm = str(Header(nm, encoding))

    ---
    
    > # This try-except clause is needed on Python 3 < 3.2.4
    > # http://bugs.python.org/issue14291
    > try:
    > nm = Header(nm, encoding).encode()
    > except UnicodeEncodeError:
    > nm = Header(nm, 'utf-8').encode()

::

   113c118
   
   < addr = str(Header(addr, encoding))
   
   ---
   
   > addr = Header(addr, encoding).encode()


アドレスのエンコード化も変更
------------------------------

::

    105c110

    < addr = addr.encode('ascii')

    ---

    > addr.encode('ascii')



ドメインのエンコード化も変更
------------------------------------

::


    110c115

    < domain = domain.encode('idna')

    ---
    
    > domain = domain.encode('idna').decode('ascii')





ペイロードのエンコード化の修正
----------------------------------------------------


::

   136a142,144
   
   > if sys.version_info < (2, 6, 6) and isinstance(self._payload, six.text_type):
   > # Workaround for http://bugs.python.org/issue1368247
   > self._payload = self._payload.encode(self._charset.output_charset)
   

::

    213,214c221

    < msg = SafeMIMEText(smart_str(self.body, encoding),
    < self.content_subtype, encoding)

    ---

    > msg = SafeMIMEText(self.body, self.content_subtype, encoding)

::

    295c303
    
    < attachment = SafeMIMEText(smart_str(content, encoding), subtype, encoding)
    
    ---
    
    > attachment = SafeMIMEText(content, subtype, encoding)
    


withをつかったIO
---------------------


::

    268c275,276
    
    < content = open(path, 'rb').read()
    
    ---
    
    > with open(path, 'rb') as f:
    > content = f.read()
    



添付ファイル名のエンコード対応
---------------------------------------

::

    315c323

    < filename = filename.encode('ascii')

    ---

    > filename.encode('ascii')


::

    317c325,327
    
    < filename = ('utf-8', '', filename.encode('utf-8'))
    
    ---
    
    > if not six.PY3:
    > filename = filename.encode('utf-8')
    > filename = ('utf-8', '', filename)

