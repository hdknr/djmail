=================================================
django.core.mail: __init__.py
=================================================

.. contents::
    :local:

リテラルのデフォルトUnicode化
---------------------------------------

::

    3a4
    > from __future__ import unicode_literals

::

    91c93
    < mail = EmailMultiAlternatives(u'%s%s' % (settings.EMAIL_SUBJECT_PREFIX, subject),
    ---
    > mail = EmailMultiAlternatives('%s%s' % (settings.EMAIL_SUBJECT_PREFIX, subject),
    104c106
    < mail = EmailMultiAlternatives(u'%s%s' % (settings.EMAIL_SUBJECT_PREFIX, subject),
    ---
    > mail = EmailMultiAlternatives('%s%s' % (settings.EMAIL_SUBJECT_PREFIX, subject),

Python 3互換の例外
------------------------

::

    33c34
    < except ImportError, e:
    ---
    > except ImportError as e:

EmailMessageにconnectionを指定
------------------------------------------

- send_mass_mailで個別のメッセージ( EmailMessage )を作る際に connection を引数に設定するようになりました (1.4で入れ忘れていた？ )

::

    81c82,83
    < messages = [EmailMessage(subject, message, sender, recipient)
    ---
    > messages = [EmailMessage(subject, message, sender, recipient,
    > connection=connection)


