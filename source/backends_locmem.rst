=============================================
django.core.mail.backends.locmem
=============================================

- ローカルメモリ: テスト用

.. contents::
    :local:


Django 1.4->1.5RC
======================

- send_messagesにメールのリストが渡ってきますが、各メールのヘッダーの検証をするようになりました


.. code-block:: python

    def send_messages(self, messages):
        """Redirect messages to the dummy outbox"""

        for message in messages: # .message() triggers header validation        #:追加
            message.message()                                                   #:追加

        mail.outbox.extend(messages)
        return len(messages)


